package io.renren.modules.zjwl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.zjwl.entity.DeviceAgentEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-05-13 20:40:41
 */
public interface DeviceAgentService extends IService<DeviceAgentEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

