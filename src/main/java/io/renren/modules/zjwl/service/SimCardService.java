package io.renren.modules.zjwl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.zjwl.entity.SimCardEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-05-01 13:18:46
 */
public interface SimCardService extends IService<SimCardEntity> {

    PageUtils queryPage(Map<String, Object> params);
    boolean updateExpireDateBatch(SimCardEntity simCardEntity);
    boolean importSimCard(MultipartFile file,Integer agentUserId);
    void outputSimCard(List<Integer> simcardIds);
    boolean bindDeviceIdBySimCode(String deviceId,String simCode);
}

