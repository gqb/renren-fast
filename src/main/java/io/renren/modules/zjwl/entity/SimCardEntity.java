package io.renren.modules.zjwl.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-05-01 13:18:46
 */
@Data
@TableName("sim_card")
public class SimCardEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * sim卡号
	 */
	private String simCode;
	private String mobile;
	/**
	 * 激活时间
	 */
	private Date activedate;
	/**
	 * 剩余天数
	 */
	private Integer dayLeftCount;
	private Date expireDate;
	/**
	 * 设备id
	 */
	private String deviceId;
	/**
	 * 代理商用户id
	 */
	private Integer agentUserId;

	@TableField(exist = false)
	private String agentUsername;

	@TableField(exist = false)
	private String agentName;

	private Integer agentId;



	@TableField(exist = false)
	private List<Integer> simcardIds=new ArrayList<>();
}
