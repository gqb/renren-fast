package io.renren.modules.zjwl.dao;

import io.renren.modules.zjwl.entity.SimCardEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-05-01 13:18:46
 */
@Mapper
public interface SimCardDao extends BaseMapper<SimCardEntity> {

    @Select("select distinct device_id from sim_card where agent_id =#{agentId}")
    List<String> findDeviceIdByAgentId(@Param("agentId") int agentId);


}
