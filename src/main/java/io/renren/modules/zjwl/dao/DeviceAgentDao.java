package io.renren.modules.zjwl.dao;

import io.renren.modules.zjwl.entity.DeviceAgentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-05-13 20:40:41
 */
@Mapper
public interface DeviceAgentDao extends BaseMapper<DeviceAgentEntity> {
	
}
