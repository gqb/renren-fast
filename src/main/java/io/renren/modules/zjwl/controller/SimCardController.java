package io.renren.modules.zjwl.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.exception.RRException;
import io.renren.modules.zjwl.utils.SimcardOverdueUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.zjwl.entity.SimCardEntity;
import io.renren.modules.zjwl.service.SimCardService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-05-01 13:18:46
 */
@RestController
@RequestMapping("/simcard")
public class SimCardController {
    @Autowired
    private SimCardService simCardService;

    @RequestMapping("/overdue")
    public R overdueInfo(){
        return R.ok().put("data", SimcardOverdueUtil.overdueSimcardMap);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("generator:simcard:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = simCardService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/importSimCardExcel")
    public R importSimCardExcel(@RequestParam("file") MultipartFile file, @RequestParam("agentUserId") Integer agentUserId) {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        if (suffix.equalsIgnoreCase(".xlsx")
                || suffix.equalsIgnoreCase(".xls")) {
            boolean result = simCardService.importSimCard(file,agentUserId);
            if (!result) {
                throw new RRException("导入sim失败");
            }
        } else {
            throw new RRException("上传文件不是excel格式");
        }
        return R.ok();
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("generator:simcard:info")
    public R info(@PathVariable("id") Integer id) {
        SimCardEntity simCard = simCardService.getById(id);

        return R.ok().put("simCard", simCard);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("generator:simcard:save")
    public R save(@RequestBody SimCardEntity simCard) {
        simCardService.save(simCard);

        return R.ok();
    }

    @RequestMapping("/updateBatch")
    public R updateExpireDateBatch(@RequestBody SimCardEntity simCard){
        System.out.println(simCard.toString());
        if(simCardService.updateExpireDateBatch(simCard)){
            return R.ok();
        }
        return R.ok();
    }
    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:simcard:update")
    public R update(@RequestBody SimCardEntity simCard) {
        simCardService.updateById(simCard);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:simcard:delete")
    public R delete(@RequestBody Integer[] ids) {
        simCardService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
