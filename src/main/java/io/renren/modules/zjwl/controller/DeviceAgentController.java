package io.renren.modules.zjwl.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.zjwl.entity.DeviceAgentEntity;
import io.renren.modules.zjwl.service.DeviceAgentService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-05-13 20:40:41
 */
@RestController
@RequestMapping("/deviceagent")
public class DeviceAgentController {
    @Autowired
    private DeviceAgentService deviceAgentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("generator:deviceagent:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = deviceAgentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("generator:deviceagent:info")
    public R info(@PathVariable("id") Integer id){
		DeviceAgentEntity deviceAgent = deviceAgentService.getById(id);

        return R.ok().put("deviceAgent", deviceAgent);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("generator:deviceagent:save")
    public R save(@RequestBody DeviceAgentEntity deviceAgent){
        deviceAgent.setCreateTime(new Date());
		deviceAgentService.save(deviceAgent);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:deviceagent:update")
    public R update(@RequestBody DeviceAgentEntity deviceAgent){

		deviceAgentService.updateById(deviceAgent);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:deviceagent:delete")
    public R delete(@RequestBody Integer[] ids){
		deviceAgentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
