package io.renren.modules.zjwl.utils;

import io.renren.modules.zjwl.entity.SimCardEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimcardOverdueUtil {

    /**
     * key  日期
     * value  队列
     */
    public static Map<String, List<SimCardEntity>> overdueSimcardMap = new HashMap<>();


    public static void put(String key, List<SimCardEntity> simCardEntityList) {
        overdueSimcardMap.put(key,simCardEntityList);
    }

    public static void delete(String key) {
        overdueSimcardMap.remove(key);

    }

    public static List<SimCardEntity> get(String key) {

        return overdueSimcardMap.getOrDefault(key, new ArrayList<>());
    }

}
