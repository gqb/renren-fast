/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren.modules.app.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.exception.RRException;
import io.renren.common.validator.Assert;
import io.renren.modules.app.dao.UserDao;
import io.renren.modules.app.entity.UserEntity;
import io.renren.modules.app.form.LoginForm;
import io.renren.modules.app.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Override
    public UserEntity queryByMobile(String mobile) {
        return baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("mobile", mobile));
    }

	@Override
	public UserEntity queryByUsername(String username) {
		return baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("username", username));

	}

	@Override
    public UserEntity login(LoginForm form) {
        UserEntity user = null;
        if (StringUtils.isNotEmpty(form.getUsername())) {
            user = baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("username", form.getUsername()));
        } else if (StringUtils.isNotEmpty(form.getMobile())) {
            user = queryByMobile(form.getMobile());
        }
        Assert.isNull(user, "手机号、用户名或密码错误");

        //密码错误
        if (!user.getPassword().equals(DigestUtils.sha256Hex(form.getPassword()))) {
            throw new RRException("手机号或密码错误");
        }

        return user;
    }
}
