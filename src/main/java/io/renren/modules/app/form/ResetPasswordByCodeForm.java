/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.app.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 注册表单
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
@ApiModel(value = "重置密码表单")
public class ResetPasswordByCodeForm {


    @ApiModelProperty(value = "验证码")
    @NotBlank(message="验证码不能为空")
    private String code;
    @ApiModelProperty(value = "密码")
    @NotBlank(message="新密码不能为空")
    private String newPassword;




}
