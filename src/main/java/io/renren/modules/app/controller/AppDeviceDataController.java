package io.renren.modules.app.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.device.entity.DeviceDataEntity;
import io.renren.modules.device.service.DeviceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-17 14:32:55
 */
@RestController
@RequestMapping("/app/deviceData")
public class AppDeviceDataController {
    @Autowired
    private DeviceDataService deviceDataService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("generator:devicedata:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = deviceDataService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("generator:devicedata:info")
    public R info(@PathVariable("id") Integer id){
		DeviceDataEntity deviceData = deviceDataService.getById(id);

        return R.ok().put("deviceData", deviceData);
    }

    /**
     * 保存
     */
//    @RequestMapping("/save")
//    @RequiresPermissions("generator:devicedata:save")
    public R save(@RequestBody DeviceDataEntity deviceData){
		deviceDataService.save(deviceData);

        return R.ok();
    }

    /**
     * 修改
     */
//    @RequestMapping("/update")
//    @RequiresPermissions("generator:devicedata:update")
    public R update(@RequestBody DeviceDataEntity deviceData){
		deviceDataService.updateById(deviceData);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:devicedata:delete")
    public R delete(@RequestBody Integer[] ids){
		deviceDataService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
