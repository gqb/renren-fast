/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.app.controller;


import cn.hutool.core.util.ObjectUtil;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.app.annotation.Login;
import io.renren.modules.app.entity.UserEntity;
import io.renren.modules.app.form.RegisterForm;
import io.renren.modules.app.form.ResetPasswordByCodeForm;
import io.renren.modules.app.form.ResetPasswordForm;
import io.renren.modules.app.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * 注册
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/app")
@Api("APP注册接口")
public class AppRegisterController {
    @Autowired
    private UserService userService;

    @PostMapping("register")
    @ApiOperation("注册")
    public R register(@RequestBody RegisterForm form){
        //表单校验
        ValidatorUtils.validateEntity(form);
        UserEntity userEntity= userService.queryByMobile(form.getMobile());
        if(ObjectUtil.isNotNull(userEntity)){
            return R.error(500,"该手机号已经注册");
        }
        userEntity=userService.queryByUsername(form.getUsername());
        if(ObjectUtil.isNotNull(userEntity)){
            return R.error(500,"该用户名已经注册");
        }

        UserEntity user = new UserEntity();
        user.setMobile(form.getMobile());
        user.setUsername(form.getUsername());
        user.setPassword(DigestUtils.sha256Hex(form.getPassword()));
        user.setCreateTime(new Date());
        userService.save(user);

        return R.ok();
    }
    @Login
    @PostMapping("resetPassword")
    @ApiOperation("重置密码")
    public R resetPassword(@RequestBody ResetPasswordForm form, HttpServletRequest request){
        ValidatorUtils.validateEntity(form);
        UserEntity userEntity=userService.getById((Long) request.getAttribute("userId"));
        if(ObjectUtil.isNull(userEntity)){
            return R.error(500,"不存在该用户,请重新登录");
        }
        if(!DigestUtils.sha256Hex(form.getPassword()).equalsIgnoreCase(userEntity.getPassword())){
            return R.error(500,"原密码错误");
        }
        userEntity.setPassword(DigestUtils.sha256Hex(form.getNewPassword()));
        userService.updateById(userEntity);
        return R.ok();
    }

    @Login
    @PostMapping("resetPasswordByCode")
    @ApiOperation("重置密码")
    public R resetPasswordByCode(@RequestBody ResetPasswordByCodeForm form, HttpServletRequest request){
        ValidatorUtils.validateEntity(form);
        UserEntity userEntity=userService.getById((Long) request.getAttribute("userId"));
        if(ObjectUtil.isNull(userEntity)){
            return R.error(500,"不存在该用户,请重新登录");
        }
        userEntity.setPassword(DigestUtils.sha256Hex(form.getNewPassword()));
        userService.updateById(userEntity);
        return R.ok();
    }
}
