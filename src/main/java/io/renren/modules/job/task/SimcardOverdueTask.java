package io.renren.modules.job.task;


import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.renren.modules.zjwl.entity.SimCardEntity;
import io.renren.modules.zjwl.service.impl.SimCardServiceImpl;
import io.renren.modules.zjwl.utils.SimcardOverdueUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//@Component("simcardOverDueTask")
public class SimcardOverdueTask implements ITask {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SimCardServiceImpl simCardService;
    @PostConstruct
    public void init(){
        run("");
    }
    @Override
    public void run(String params) {
        List<SimCardEntity> simCardEntityList = simCardService.list();
        List<SimCardEntity> expireSimcardList = new ArrayList<>();
        List<SimCardEntity> expireIn7List=new ArrayList<>();
        List<SimCardEntity> expireIn30List=new ArrayList<>();
        simCardEntityList.forEach(simCardEntity -> {
            try{
                Date now=new Date();
                //判断是否过期
                if (simCardEntity.getExpireDate().before(new Date())) {
                    expireSimcardList.add(simCardEntity);
                    logger.info(simCardEntity.getSimCode()+"已经过期");
                }
                Date expireIn7Day=DateUtil.offsetDay(simCardEntity.getExpireDate(),-7);
                if(now.after(expireIn7Day)&&now.before(simCardEntity.getExpireDate())){
                    expireIn7List.add(simCardEntity);
                    logger.info(simCardEntity.getSimCode()+"7天内过期");
                }else{
                    Date expireIn30Day=DateUtil.offsetDay(simCardEntity.getExpireDate(),-15);
                    if(now.after(expireIn30Day)&&now.before(simCardEntity.getExpireDate())){
                        expireIn30List.add(simCardEntity);
                        logger.info(simCardEntity.getSimCode()+"15天内过期");
                    }
                }

            }catch (Exception e){
                logger.error(e.getMessage());
            }
        });

        simCardService.compareAgentName(expireSimcardList);
        SimcardOverdueUtil.put("over",expireSimcardList);
        simCardService.compareAgentName(expireIn7List);
        SimcardOverdueUtil.put("seven",expireIn7List);
        simCardService.compareAgentName(expireIn30List);
        SimcardOverdueUtil.put("thirty",expireIn30List);


    }


}
