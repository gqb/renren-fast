package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 数据字典详情
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-22 14:25:40
 */
@Data
@TableName("sys_dict_detail")
public class SysDictDetailEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long detailId;
	/**
	 * 字典id
	 */
	private Long dictId;
	/**
	 * 字典标签
	 */
	private String label;
	/**
	 * 字典值
	 */
	private String value;
	/**
	 * 排序
	 */
	private Integer dictSort;
	/**
	 * 创建者
	 */
	private String createBy;
	/**
	 * 更新者
	 */
	private String updateBy;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;

	private BigDecimal threshold;

	private Integer lineCount;

	private String config;

	@TableField(exist = false)
	private List<String> lineNameData=new ArrayList<>();
}
