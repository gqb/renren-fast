package io.renren.modules.sys.controller;

import java.util.*;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.sys.entity.SysDictDetailEntity;
import io.renren.modules.sys.service.SysDictDetailService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;


/**
 * 数据字典详情
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-22 14:25:40
 */
@RestController
@RequestMapping("sys/sysdictdetail")
public class SysDictDetailController {
    @Autowired
    private SysDictDetailService sysDictDetailService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("generator:sysdictdetail:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysDictDetailService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{detailId}")
//    @RequiresPermissions("generator:sysdictdetail:info")
    public R info(@PathVariable("detailId") Long detailId) {
        SysDictDetailEntity sysDictDetail = sysDictDetailService.getById(detailId);
        if (JSONUtil.isJson(sysDictDetail.getConfig())) {
            JSONObject jsonObject = JSON.parseObject(sysDictDetail.getConfig());
            List<String> lineNameData = jsonObject.getJSONArray("lineNameData").toJavaList(String.class);
            sysDictDetail.setLineNameData(lineNameData);
        }
        return R.ok().put("sysDictDetail", sysDictDetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("generator:sysdictdetail:save")
    public R save(@RequestBody SysDictDetailEntity sysDictDetail) {
        sysDictDetail.setCreateTime(new Date());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("lineNameData"
                , sysDictDetail.getLineNameData());
        sysDictDetail.setConfig(jsonObject.toJSONString());
        sysDictDetailService.save(sysDictDetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:sysdictdetail:update")
    @Transactional
    public R update(@RequestBody SysDictDetailEntity sysDictDetail) {
        sysDictDetail.setUpdateTime(new Date());
        JSONObject jsonObject = JSON.parseObject("{}");
        jsonObject.put("lineNameData"
                , sysDictDetail.getLineNameData());
        sysDictDetail.setConfig(jsonObject.toJSONString());
        sysDictDetailService.updateById(sysDictDetail);

        return R.ok();
    }

    public static void main(String[] args) {
        List<String> strs = new ArrayList<>();
        strs.add("fadfa");
        strs.add("fadfa");
        strs.add("dafadf");
        System.out.println(strs.toString());
        JSONArray jsonArray = new JSONArray();
        jsonArray.add("dada");
        jsonArray.add("dada");
        jsonArray.add("dada");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("lineNameData"
                , strs);
        System.out.println(jsonObject.toJSONString());
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:sysdictdetail:delete")
    public R delete(@RequestBody Long[] detailIds) {
        sysDictDetailService.removeByIds(Arrays.asList(detailIds));

        return R.ok();
    }

}
