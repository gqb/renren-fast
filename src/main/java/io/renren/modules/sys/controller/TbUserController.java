package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import cn.hutool.core.util.ObjectUtil;
import io.renren.modules.app.entity.UserEntity;
import io.renren.modules.app.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.sys.entity.TbUserEntity;
import io.renren.modules.sys.service.TbUserService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 用户
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-23 12:09:17
 */
@RestController
@RequestMapping("/user")
public class TbUserController {
    @Autowired
    private TbUserService userService;
    @Autowired
    private UserService appUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("generator:user:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{userId}")
//    @RequiresPermissions("generator:user:info")
    public R info(@PathVariable("userId") Long userId){
		TbUserEntity user = userService.getById(userId);

        return R.ok().put("user", user);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("generator:user:save")
    public R save(@RequestBody TbUserEntity user){
        UserEntity userEntity= appUserService.queryByMobile(user.getMobile());
        if(ObjectUtil.isNotNull(userEntity)){
            return R.error(500,"该手机号已经注册");
        }
        user.setCreateTime(new Date());
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        userService.save(user);
        return R.ok();
    }
    @RequestMapping("/resetPassword")
//    @RequiresPermissions("generator:user:save")
    public R resetPassword(@RequestBody TbUserEntity user){
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        if(userService.resetPassword(user)){
            return R.ok();
        }else{
            return R.error(500,"重置密码失败");
        }
    }
    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:user:update")
    public R update(@RequestBody TbUserEntity user){
        UserEntity userEntity= appUserService.queryByMobile(user.getMobile());
        if(ObjectUtil.isNotNull(userEntity)){
            if(!userEntity.getUserId().equals(user.getUserId())){
                return R.error(500,"修改的手机号和已经注册的手机号重复");
            }
        }
		userService.updateById(user);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:user:delete")
    public R delete(@RequestBody Long[] userIds){
		userService.removeByIds(Arrays.asList(userIds));

        return R.ok();
    }

}
