package io.renren.modules.sys.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.renren.modules.sys.entity.SysDictEntity;
import io.renren.modules.sys.service.SysDictService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.sys.dao.SysDictDetailDao;
import io.renren.modules.sys.entity.SysDictDetailEntity;
import io.renren.modules.sys.service.SysDictDetailService;


@Service("sysDictDetailService")
public class SysDictDetailServiceImpl extends ServiceImpl<SysDictDetailDao, SysDictDetailEntity> implements SysDictDetailService {

    @Autowired
    private SysDictService sysDictService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SysDictDetailEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("label", params.getOrDefault("label", "").toString());
        queryWrapper.like("value", params.getOrDefault("value", "").toString());
        if (params.containsKey("dictId") && NumberUtil.isNumber(params.get("dictId").toString())) {
            queryWrapper.eq("dict_id", Long.parseLong(params.get("dictId").toString()));
        }
        IPage<SysDictDetailEntity> page = this.page(
                new Query<SysDictDetailEntity>().getPage(params),
                queryWrapper
        );
        compareLineNameData(page.getRecords());
        return new PageUtils(page);
    }

    public void compareLineNameData(List<SysDictDetailEntity> sysDictDetailEntities) {
        sysDictDetailEntities.forEach(sysDictDetailEntity -> {
            if (JSONUtil.isJson(sysDictDetailEntity.getConfig())) {
                JSONObject jsonObject = JSON.parseObject(sysDictDetailEntity.getConfig());
                List<String> lineNameData = jsonObject.getJSONArray("lineNameData").toJavaList(String.class);
                sysDictDetailEntity.setLineNameData(lineNameData);
            }
        });
    }

    @Override
    public List<SysDictDetailEntity> findByDictName(String name) {
        if (StringUtils.isBlank(name)) {
            return new ArrayList<>();
        }
        QueryWrapper<SysDictEntity> sysDictEntityQueryWrapper = new QueryWrapper<>();
        sysDictEntityQueryWrapper.like("name", name);
        List<SysDictEntity> dictEntities = sysDictService.list(sysDictEntityQueryWrapper);
        if (CollectionUtil.isEmpty(dictEntities)) {
            return new ArrayList<>();
        }
        Long dictId = dictEntities.get(0).getDictId();
        QueryWrapper<SysDictDetailEntity> detailEntityQueryWrapper = new QueryWrapper<>();
        detailEntityQueryWrapper.eq("dict_id", dictId);

        List<SysDictDetailEntity> dictDetailEntities = list(detailEntityQueryWrapper);

        return dictDetailEntities;

    }

}