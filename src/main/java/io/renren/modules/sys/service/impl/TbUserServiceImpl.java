package io.renren.modules.sys.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.device.dao.DeviceDao;
import io.renren.modules.device.entity.DeviceEntity;
import io.renren.modules.device.service.DeviceService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.zjwl.dao.SimCardDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.sys.dao.TbUserDao;
import io.renren.modules.sys.entity.TbUserEntity;
import io.renren.modules.sys.service.TbUserService;


@Service("tbUserService")
public class TbUserServiceImpl extends ServiceImpl<TbUserDao, TbUserEntity> implements TbUserService {
    @Autowired
    private SimCardDao simCardDao;

    @Autowired
    private DeviceService deviceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<TbUserEntity> queryWrapper = new QueryWrapper<>();
        if (params.containsKey("mobile") && StringUtils.isNotEmpty((String) params.get("mobile"))) {
            queryWrapper.like("mobile", params.get("mobile"));
        }
        if (params.containsKey("username") &&
                StringUtils.isNotEmpty((String) params.get("username"))) {
            queryWrapper.like("username", params.get("username"));
        }
        if (isAgent()) {
            List<String> deviceIdStrList = simCardDao.findDeviceIdByAgentId(ShiroUtils.getUserEntity().getAgentId());
            if(CollectionUtil.isNotEmpty(deviceIdStrList)){
                QueryWrapper<DeviceEntity> deviceQuery = new QueryWrapper<>();
                deviceQuery.in("lot_device_id",deviceIdStrList);
                deviceQuery.select("distinct user_id");
                List<DeviceEntity> deviceEntities= deviceService.list(deviceQuery);
                if(CollectionUtil.isNotEmpty(deviceEntities)){
                    queryWrapper.in("user_id",deviceEntities.stream().map(DeviceEntity::getUserId).collect(Collectors.toList()));
                }
            }

        }
        IPage<TbUserEntity> page = this.page(
                new Query<TbUserEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    public boolean isAgent() {
        SysUserEntity userEntity = ShiroUtils.getUserEntity();
        if (userEntity.getAgentId() != 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean resetPassword(TbUserEntity userEntity) {
        UpdateWrapper<TbUserEntity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("user_id", userEntity.getUserId());
        updateWrapper.set("password", userEntity.getPassword());
        boolean result = update(updateWrapper);
        return result;
    }

}