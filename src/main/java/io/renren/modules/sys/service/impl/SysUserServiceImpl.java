/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.exception.RRException;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.SysUserDao;
import io.renren.modules.sys.entity.SysRoleEntity;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.entity.SysUserRoleEntity;
import io.renren.modules.sys.service.SysRoleService;
import io.renren.modules.sys.service.SysUserRoleService;
import io.renren.modules.sys.service.SysUserService;
import io.renren.modules.zjwl.entity.DeviceAgentEntity;
import io.renren.modules.zjwl.service.DeviceAgentService;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 系统用户
 *
 * @author Mark sunlightcs@gmail.com
 */
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService {
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private DeviceAgentService deviceAgentService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String username = (String) params.get("username");
        Long createUserId = (Long) params.get("createUserId");
        QueryWrapper<SysUserEntity> queryWrapper = new QueryWrapper<SysUserEntity>()
                .like(StringUtils.isNotBlank(username), "username", username)
                .eq(createUserId != null, "create_user_id", createUserId);
        if (params.containsKey("roleName")) {
            List<Long> userIds = getUserIdByRoleName(params.get("roleName").toString());
            queryWrapper.in("user_id", userIds);
        }
        if (params.containsKey("agentId") && NumberUtil.isNumber(params.get("agentId").toString())) {
            queryWrapper.eq("agent_id",
                    Integer.parseInt(params.get("agentId").toString()));
        }
        IPage<SysUserEntity> page = this.page(
                new Query<SysUserEntity>().getPage(params),
                queryWrapper
        );
        compareAgentName(page.getRecords());
        return new PageUtils(page);
    }

    private void compareAgentName(List<SysUserEntity> sysUserEntityList) {
        if (CollectionUtil.isEmpty(sysUserEntityList)) {
            return;
        }
        Set<Integer> agentIds = sysUserEntityList.stream()
                .map(SysUserEntity::getAgentId).collect(Collectors.toSet());

        QueryWrapper<DeviceAgentEntity> agentQueryWrapper = new QueryWrapper<>();
        agentQueryWrapper.in("id", new ArrayList<>(agentIds));
        List<DeviceAgentEntity> deviceAgentEntities = deviceAgentService.list(agentQueryWrapper);
        Map<Integer, String> agentNameMap = deviceAgentEntities
                .stream().collect(Collectors.toMap(DeviceAgentEntity::getId, DeviceAgentEntity::getName));

        sysUserEntityList.forEach(sysUserEntity -> {
            if(sysUserEntity.getAgentId()!=0
                    &&agentNameMap.containsKey(sysUserEntity.getAgentId())){
                sysUserEntity.setAgentName(agentNameMap.get(sysUserEntity.getAgentId()));
            }
        });

    }

    @Override
    public List<String> queryAllPerms(Long userId) {
        return baseMapper.queryAllPerms(userId);
    }

    @Override
    public List<Long> queryAllMenuId(Long userId) {
        return baseMapper.queryAllMenuId(userId);
    }

    @Override
    public SysUserEntity queryByUserName(String username) {
        return baseMapper.queryByUserName(username);
    }

    @Override
    @Transactional
    public void saveUser(SysUserEntity user) {
        user.setCreateTime(new Date());
        //sha256加密
        String salt = RandomStringUtils.randomAlphanumeric(20);
        user.setPassword(new Sha256Hash(user.getPassword(), salt).toHex());
        user.setSalt(salt);
        this.save(user);

        //检查角色是否越权
        checkRole(user);

        //保存用户与角色关系
        sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
    }

    @Override
    @Transactional
    public void update(SysUserEntity user) {
        if (StringUtils.isBlank(user.getPassword())) {
            user.setPassword(null);
        } else {
            user.setPassword(new Sha256Hash(user.getPassword(), user.getSalt()).toHex());
        }
        this.updateById(user);

        //检查角色是否越权
        checkRole(user);

        //保存用户与角色关系
        sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
    }

    @Override
    public void deleteBatch(Long[] userId) {
        this.removeByIds(Arrays.asList(userId));
    }

    @Override
    public boolean updatePassword(Long userId, String password, String newPassword) {
        SysUserEntity userEntity = new SysUserEntity();
        userEntity.setPassword(newPassword);
        return this.update(userEntity,
                new QueryWrapper<SysUserEntity>().eq("user_id", userId).eq("password", password));
    }

    @Override
    public List<Long> getUserIdByRoleName(String roleName) {
        QueryWrapper<SysRoleEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_name", roleName);
        List<SysRoleEntity> roleEntities = sysRoleService.list(queryWrapper);
        if (CollectionUtil.isEmpty(roleEntities)) {
            return new ArrayList<>();
        }
        QueryWrapper<SysUserRoleEntity> userRoleEntityQueryWrapper = new QueryWrapper<>();
        userRoleEntityQueryWrapper.eq("role_id", roleEntities.get(0).getRoleId());
        List<SysUserRoleEntity> userRoleEntities = sysUserRoleService.list(userRoleEntityQueryWrapper);
        if (CollectionUtil.isEmpty(userRoleEntities)) {
            return new ArrayList<>();
        }
        return userRoleEntities.stream().map(SysUserRoleEntity::getUserId).collect(Collectors.toList());
    }

    /**
     * 检查角色是否越权
     */
    private void checkRole(SysUserEntity user) {
        if (user.getRoleIdList() == null || user.getRoleIdList().size() == 0) {
            return;
        }
        //如果不是超级管理员，则需要判断用户的角色是否自己创建
        if (user.getCreateUserId() == Constant.SUPER_ADMIN) {
            return;
        }

        //查询用户创建的角色列表
        List<Long> roleIdList = sysRoleService.queryRoleIdList(user.getCreateUserId());

        //判断是否越权
        if (!roleIdList.containsAll(user.getRoleIdList())) {
            throw new RRException("新增用户所选角色，不是本人创建");
        }
    }
}