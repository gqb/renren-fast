package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.SysDictDetailEntity;

import java.util.List;
import java.util.Map;

/**
 * 数据字典详情
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-22 14:44:58
 */
public interface SysDictDetailService extends IService<SysDictDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
    public List<SysDictDetailEntity> findByDictName(String name);

}

