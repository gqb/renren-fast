package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.TbUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-23 12:09:17
 */
@Mapper
public interface TbUserDao extends BaseMapper<TbUserEntity> {
	
}
