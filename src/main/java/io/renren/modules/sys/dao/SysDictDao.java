package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.SysDictEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-22 14:25:40
 */
@Mapper
public interface SysDictDao extends BaseMapper<SysDictEntity> {
	
}
