package io.renren.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.device.entity.DeviceEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-16 14:04:41
 */
public interface DeviceService extends IService<DeviceEntity> {

    boolean checkDeviceBind(String lotDeviceId);
    PageUtils queryPage(Map<String, Object> params);
    PageUtils queryPageAdmin(Map<String, Object> params);
}

