package io.renren.modules.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.device.entity.DeviceDataEntity;

import java.util.Map;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-17 14:32:55
 */
public interface DeviceDataService extends IService<DeviceDataEntity> {

    PageUtils queryPage(Map<String, Object> params);

    int deviceOffline(int minutes,String deviceId);

    Map<String,Object> deviceDate(Map<String,Object> params);
}

