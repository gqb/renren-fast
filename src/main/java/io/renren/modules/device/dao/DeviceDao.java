package io.renren.modules.device.dao;

import io.renren.modules.device.entity.DeviceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-16 14:04:41
 */
@Mapper
public interface DeviceDao extends BaseMapper<DeviceEntity> {

}
