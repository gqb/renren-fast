package io.renren.modules.device.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.sys.entity.SysDictDetailEntity;
import io.renren.modules.sys.service.SysDictDetailService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 数据字典详情
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-22 14:25:40
 */
@RestController
@RequestMapping("app/dictDetail")
public class AppDictDetailController {
    @Autowired
    private SysDictDetailService sysDictDetailService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("generator:sysdictdetail:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDictDetailService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/listByDictName")
//    @RequiresPermissions("generator:sysdictdetail:list")
    public R listByDictName(@RequestParam String name){
        return R.ok().put("data", sysDictDetailService.findByDictName(name));
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:sysdictdetail:save")
    public R save(@RequestBody SysDictDetailEntity sysDictDetail){
		sysDictDetailService.save(sysDictDetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:sysdictdetail:update")
    public R update(@RequestBody SysDictDetailEntity sysDictDetail){
		sysDictDetailService.updateById(sysDictDetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:sysdictdetail:delete")
    public R delete(@RequestBody Long[] detailIds){
		sysDictDetailService.removeByIds(Arrays.asList(detailIds));

        return R.ok();
    }

}
