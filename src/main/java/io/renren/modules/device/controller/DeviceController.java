package io.renren.modules.device.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import io.renren.common.utils.ShiroUtils;
import io.renren.modules.app.annotation.Login;
import io.renren.modules.zjwl.service.SimCardService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.device.entity.DeviceEntity;
import io.renren.modules.device.service.DeviceService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-16 14:04:41
 */
@RestController
@RequestMapping("/device")
public class DeviceController {
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private SimCardService simCardService;
    /**
     * 列表
     */
    @GetMapping("/list")
//    @RequiresPermissions("generator:device:list")
    public R list(@RequestParam Map<String, Object> params){
        System.out.println(ShiroUtils.getUserId());
        PageUtils page = deviceService.queryPageAdmin(params);

        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("generator:device:info")
    public R info(@PathVariable("id") Integer id){
		DeviceEntity device = deviceService.getById(id);

        return R.ok().put("data", device);
    }

    /**
     * 保存
     */
    @Login
    @PostMapping("/save")
//    @RequiresPermissions("generator:device:save")
    public R save(@RequestBody DeviceEntity device){
        device.setCreateTime(new Date());
        String address="";
        if (StringUtils.isNotBlank(device.getProvince())) {
            address=device.getProvince();
        }
        if (StringUtils.isNotBlank(device.getCity())) {
            address=address+device.getCity();
        }
        if (StringUtils.isNotBlank(device.getArea())) {
            address=address+device.getArea();
        }
        device.setAddress(address);
        simCardService.bindDeviceIdBySimCode(device.getLotDeviceId(),device.getSimCode());
		deviceService.save(device);

        return R.ok().put("data",device);
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:device:update")
    public R update(@RequestBody DeviceEntity device){
        device.setUpdateTime(new Date());
        String address="";
        if (StringUtils.isNotBlank(device.getProvince())) {
            address=device.getProvince();
        }
        if (StringUtils.isNotBlank(device.getCity())) {
            address=address+device.getCity();
        }
        if (StringUtils.isNotBlank(device.getArea())) {
            address=address+device.getArea();
        }
        device.setAddress(address);
		deviceService.updateById(device);
        simCardService.bindDeviceIdBySimCode(device.getLotDeviceId(),device.getSimCode());
        device= deviceService.getById(device.getId());
        return R.ok().put("data",device);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:device:delete")
    public R delete(@RequestBody Integer[] ids){
		deviceService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}
