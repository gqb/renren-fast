package io.renren.modules.device.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.device.entity.DeviceDataEntity;
import io.renren.modules.device.service.DeviceDataService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-17 14:32:55
 */
@RestController
@RequestMapping("/deviceData")
public class DeviceDataController {
    @Autowired
    private DeviceDataService deviceDataService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("generator:devicedata:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = deviceDataService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/chartData")
    public R chartData(@RequestParam Map<String, Object> params){
        return R.ok().put("data",deviceDataService.deviceDate(params));
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @RequiresPermissions("generator:devicedata:info")
    public R info(@PathVariable("id") Integer id){
		DeviceDataEntity deviceData = deviceDataService.getById(id);

        return R.ok().put("deviceData", deviceData);
    }

    /**
     * 保存
     */
//    @RequestMapping("/save")
//    @RequiresPermissions("generator:devicedata:save")
    public R save(@RequestBody DeviceDataEntity deviceData){
		deviceDataService.save(deviceData);

        return R.ok();
    }

    /**
     * 修改
     */
//    @RequestMapping("/update")
//    @RequiresPermissions("generator:devicedata:update")
    public R update(@RequestBody DeviceDataEntity deviceData){
		deviceDataService.updateById(deviceData);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:devicedata:delete")
    public R delete(@RequestBody Integer[] ids){
		deviceDataService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
