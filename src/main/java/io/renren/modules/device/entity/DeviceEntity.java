package io.renren.modules.device.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.springframework.data.annotation.Transient;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-16 14:04:41
 */
@Data
@TableName("device")
public class DeviceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主建唯一id
	 */
	@TableId
	private Integer id;
	/**
	 * 绑定的物联网设备唯一id
	 */
	private String lotDeviceId;
	/**
	 * 0控制类设备、1传感器类设备、2其他设备
	 */
	private String deviceType;
	/**
	 * 设备种类：顶膜、侧膜、遮阳帘、补光灯、风机等
	 */
	private String deviceKind;
	/**
	 * 设备名称
	 */
	private String name;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 经度
	 */
	private String longitude;
	/**
	 * 维度
	 */
	private String latitude;
	/**
	 * 创建时间
	 */
	private Date createTime;

	private Date updateTime;

	private String province;

	private String city;

	private String area;

	private String address;
	private String simCode;

	@TableField(exist = false)
	private String userName;
	@TableField(exist = false)
	private boolean online=true;
}
