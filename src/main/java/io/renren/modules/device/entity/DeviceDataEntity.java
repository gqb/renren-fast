package io.renren.modules.device.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-17 14:32:55
 */
@Data
@TableName("device_data")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceDataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private Long farmId;
	/**
	 * 
	 */
	private Long greenhouseId;
	/**
	 * 设备id sn+id
	 */
	private String deviceId;
	/**
	 * 设备类型
	 */
	private String deviceType;
	/**
	 * 
	 */
	private Long deviceCategoryId;
	/**
	 *
	 */
	private BigDecimal temp;
	private BigDecimal tempSecond;
	/**
	 * 湿度
	 */
	private BigDecimal humi;
	private BigDecimal humiSecond;
	/**
	 * 电导率
	 */
	private BigDecimal ec;
	/**
	 * 酸碱度
	 */
	private BigDecimal ph;
	/**
	 * 氮肥含量
	 */
	private BigDecimal n;
	/**
	 * 磷肥含量
	 */
	private BigDecimal p;
	/**
	 * 钾肥含量
	 */
	private BigDecimal k;
	/**
	 * 光强
	 */
	private BigDecimal light;
	/**
	 * 默认值
	 */
	private BigDecimal defaultValue;
	/**
	 * 记录状态
	 */
	private Integer status;
	/**
	 * 记录创建时间
	 */
	private Date gmtCreate;
	/**
	 * 记录更新时间
	 */
	private Date gmtModified;

	@Override
	public String toString() {
		return "DeviceDataEntity{" +
				"id=" + id +
				", farmId=" + farmId +
				", greenhouseId=" + greenhouseId +
				", deviceId='" + deviceId + '\'' +
				", deviceType='" + deviceType + '\'' +
				", deviceCategoryId=" + deviceCategoryId +
				", temp=" + temp +
				", tempSecond=" + tempSecond +
				", humi=" + humi +
				", humiSecond=" + humiSecond +
				", ec=" + ec +
				", ph=" + ph +
				", n=" + n +
				", p=" + p +
				", k=" + k +
				", light=" + light +
				", defaultValue=" + defaultValue +
				", status=" + status +
				", gmtCreate=" + gmtCreate +
				", gmtModified=" + gmtModified +
				'}';
	}
}
